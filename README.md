# Average Oracle Cleaning

The jupyter notebook contains the code for the Average-Oracle eigenvalue cleaning calibration. 

## Authors and acknowledgment
Please cite: Bongiorno, Christian, Damien Challet, and Grégoire Loeper. "Cleaning the covariance matrix of strongly nonstationary systems with time-independent eigenvalues." arXiv preprint arXiv:2111.13109 (2021).

https://arxiv.org/abs/2111.13109
